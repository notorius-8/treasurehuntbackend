# TreasureHunt application
TreasureHunt application is an android multiplayer game  
were you can explore the world around you, find hidden riddles, answer them, collect points and win.  
You can either play alone or with your friends (recommended for extra fun).

## TreasureHunt Back-end
TreasureHunt back-end is a REST api that serves the [front-end](https://gitlab.com/notorius-8/treasurehuntfrontend) application.

## Technologies
* [Java](https://www.oracle.com/java/technologies/javase-downloads.html)
* [Spring-boot](https://spring.io/projects/spring-boot)
* [Hibernate](https://hibernate.org/)
* [PostgreSQL](https://www.postgresql.org/)
* [Liquibase](https://www.liquibase.org/) for tracking, managing and applying database changes.
* [Junit 4](https://junit.org/junit4/)/ [mockito](https://site.mockito.org/)/ [hamcrest](http://hamcrest.org/JavaHamcrest/) for testing.
