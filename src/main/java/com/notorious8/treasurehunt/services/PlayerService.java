package com.notorious8.treasurehunt.services;

import com.notorious8.treasurehunt.Validator;
import com.notorious8.treasurehunt.dto.PlayerLoginDto;
import com.notorious8.treasurehunt.model.Player;
import com.notorious8.treasurehunt.repositories.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

@Service
public class PlayerService {


    private final PlayerRepository playerRepository;

    @Autowired
    public PlayerService(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    public List<Player> getAllPlayersFromRepository() throws Exception {
        if (playerRepository.count() == 0)
            throw new Exception("Player repository is empty");
        return playerRepository.findAll();
    }

    public PlayerLoginDto findByEmailAndPassword(String email, String password) throws Exception {
        Player player = playerRepository.findByEmailAndPassword(email, password);
        if (player == null)
            throw new Exception("Player not found");
        return player.convertToPlayerLoginDto();
    }

    public boolean createPlayer(Player player) throws Exception {
        Validator validator = new Validator();
        if (playerRepository.findByEmail(player.getEmail()) != null || !validator.checkPlayerValidation(player) ) {
            return false;
        }
        playerRepository.save(player);
        return true;
    }

    public Player updatePlayer(UUID id, Player playerDetails) throws Exception {
        if (playerRepository.findById(id).isEmpty()) {
            throw new Exception("Player does not exist");
        }
        Player playerWithEmailExists = playerRepository.findByEmail(playerDetails.getEmail());
        if (playerWithEmailExists != null && playerWithEmailExists.getPlayerId() != id) {
            throw new Exception("Email already in use");
        }
        Player updatedPlayer = getPlayerById(id);
        updatedPlayer.copyPlayer(playerDetails);
        playerRepository.save(updatedPlayer);
        return updatedPlayer;
    }

    public boolean deletePlayer(UUID id) throws Exception {
        Player player = getPlayerById(id);
        playerRepository.delete(player);
        return true;
    }

    public Player getPlayerById(UUID id) throws Exception {
        return playerRepository.findById(id).orElseThrow(() -> new Exception("Player " + id + " not found"));
    }
}
