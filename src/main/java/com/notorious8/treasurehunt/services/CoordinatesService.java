package com.notorious8.treasurehunt.services;

import com.notorious8.treasurehunt.exceptions.EtAuthException;
import com.notorious8.treasurehunt.exceptions.EtResourceNotFoundException;
import com.notorious8.treasurehunt.model.Coordinates;
import com.notorious8.treasurehunt.repositories.CoordinatesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class CoordinatesService {

    private final CoordinatesRepository coordinatesRepository;

    @Autowired
    public CoordinatesService(CoordinatesRepository coordinatesRepository)
    {
        this.coordinatesRepository = coordinatesRepository;
    }

    public List<Coordinates> getAListOfAllCoordinates() throws EtAuthException{
        if(coordinatesRepository.count() == 0)
            throw new EtAuthException("Repository is empty");
        return coordinatesRepository.findAll();
    }

    public List<Coordinates> getRandomCoordinates(int numberOfCoordinates) {
        if(coordinatesRepository.count() < numberOfCoordinates)
            throw new EtAuthException("Invalid Quantity");
        return coordinatesRepository.getRandomCoordinates(numberOfCoordinates);
    }

    public Coordinates getCoordinatesById(UUID id) throws EtResourceNotFoundException
    {
        return coordinatesRepository.findById(id).orElseThrow(() -> new EtResourceNotFoundException("Coordinates not found"));
    }

    public Coordinates createCoordinates(Coordinates coordinates) throws EtAuthException{
        if(coordinatesRepository.findByLongitudeAndLatitude(coordinates.getLongitude(), coordinates.getLatitude()) != null)
            throw new EtAuthException("Coordinate already exist");
        return coordinatesRepository.save(coordinates);
    }

    public Coordinates updateCoordinates(UUID id, Coordinates coordinatesDetails) throws EtResourceNotFoundException
    {
        Coordinates coordinates = coordinatesRepository.findById(id).orElseThrow(() -> new EtResourceNotFoundException("Coordinates not found"));
        coordinates.copyCoordinates(coordinatesDetails);
        return coordinatesRepository.save(coordinates);
    }

    public boolean deleteCoordinates(UUID id) throws EtResourceNotFoundException
    {
        Coordinates coordinates = coordinatesRepository.findById(id).orElseThrow(() -> new EtResourceNotFoundException("Coordinates not found"));
        coordinatesRepository.delete(coordinates);
        return true;
    }

    public long getCoordinatesRepoCount() throws EtResourceNotFoundException{
        if (coordinatesRepository.count() == 0)
            throw new EtResourceNotFoundException("Coordinates repository is empty");
        return coordinatesRepository.count();
    }


}
