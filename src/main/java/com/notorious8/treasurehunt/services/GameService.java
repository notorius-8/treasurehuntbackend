package com.notorious8.treasurehunt.services;

import com.notorious8.treasurehunt.dto.*;
import com.notorious8.treasurehunt.model.Game;
import com.notorious8.treasurehunt.model.Player;
import com.notorious8.treasurehunt.repositories.GameRepository;
import org.hibernate.mapping.Join;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class GameService {
    private final GameRepository gameRepository;

    private final PlayerService playerService;

    private final RiddleService riddleService;

    @Autowired
    public GameService(@Qualifier("fakeDao") GameRepository gameRepository, PlayerService playerService, RiddleService riddleService) {
        this.gameRepository = gameRepository;
        this.playerService = playerService;
        this.riddleService = riddleService;
    }

    public List<GameInfoDto> getAllGames() {
        List<GameInfoDto> gameInfoDtos = new ArrayList<>();
        for (Game game : gameRepository.getAllGames()) {
            gameInfoDtos.add(game.convertToGameInfoDto());
        }
        return gameInfoDtos;
    }

    public Game findGameById(String id) {
        return gameRepository.findGameById(id);
    }

    public CreateGameDto createGame(int numberOfRiddles, UUID playerId) throws Exception {
        Game newGame = gameRepository.createGame();
        newGame.setRiddleList(riddleService.getRiddleDtos(numberOfRiddles));
        int score = 0;
        for (RiddleDto r : newGame.getRiddleList()) {
            score += r.getPoints();
        }
        newGame.setScoreToWin(score);
        gameRepository.addGame(newGame);
        Player player = playerService.getPlayerById(playerId);
        newGame.addPlayer(player.convertToPlayerGameInfoDto());
        return newGame.convertToCreateGameDto();
    }

    public JoinGameDto joinGame(String gameId, UUID playerId) throws Exception {
        if (gameRepository.findGameById(gameId) == null || playerService.getPlayerById(playerId) == null)
            throw new Exception("Player or Game not found");
        Game game = gameRepository.findGameById(gameId);
        if (game.isFinished()) throw new Exception("Game is finished");
        Player player = playerService.getPlayerById(playerId);
        game.addPlayer(player.convertToPlayerGameInfoDto());
        return game.convertToJoinGameDto();
    }

    public boolean deleteGame(String id) {
        return gameRepository.deleteGame(id);
    }

    public List<RiddleDto> getGameRiddles(String id) {
        return gameRepository.findGameById(id).getRiddleList();
    }

    public int addPointsToPlayer(String gameId, UUID playerId, int points) throws Exception {
        Game game = gameRepository.findGameById(gameId);
        if (game == null || points < 0)
            throw new Exception("Game not found");
        int score = game.addPointsToPlayer(playerId, points);
        if (score >= game.getScoreToWin()) {
            finishGame(game, playerId);
        }
        return score;
    }

    public List<PlayerProgressInfoDto> getPlayersInGameInfo(String gameId) throws Exception {
        if (gameRepository.findGameById(gameId) == null) {
            throw new Exception("Game not found");
        }
        List<PlayerProgressInfoDto> playerProgressInfoDtos = new ArrayList<>();
        for (PlayerGameInfoDto playerGameInfoDto : gameRepository.findGameById(gameId).getPlayerList()) {
            playerProgressInfoDtos.add(playerGameInfoDto.convertToPlayerProgressInfoDto());
        }
        return playerProgressInfoDtos;
    }

    public GameStatusDto getGameStatus(String id) {
        return gameRepository.findGameById(id).convertToGameStatusDto();
    }

    public void finishGame(Game game, UUID playerId) throws Exception {
        game.setWinner(playerService.getPlayerById(playerId).getUserName());
        game.setFinished(true);

        TimerTask task = new TimerTask() {
            public void run() {
                deleteGame(game.getId());
            }
        };
        Timer timer = new Timer("Timer");

        long delay = 30000L; // 30 seconds
        timer.schedule(task, delay); // the game will be deleted from the memory after 30 seconds
    }
}