package com.notorious8.treasurehunt.services;

import com.notorious8.treasurehunt.dto.RiddleDto;
import com.notorious8.treasurehunt.exceptions.EtAuthException;
import com.notorious8.treasurehunt.exceptions.EtResourceNotFoundException;
import com.notorious8.treasurehunt.model.Coordinates;
import com.notorious8.treasurehunt.model.Riddle;
import com.notorious8.treasurehunt.repositories.RiddleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.*;

@Service
public class RiddleService {

    private final RiddleRepository riddleRepository;

    private final CoordinatesService coordinatesService;

    @Autowired
    public RiddleService(RiddleRepository riddleRepository, CoordinatesService coordinatesService) {
        this.coordinatesService = coordinatesService;
        this.riddleRepository = riddleRepository;
    }

    public List<Riddle> getAListOfAllRiddles() throws EtAuthException {
        if (riddleRepository.count() == 0)
            throw new EtAuthException("Repository is empty");
        return riddleRepository.findAll();
    }

    public List<Riddle> getAListOfRandomRiddles(int numberOfRiddles) throws EtAuthException {
        if (riddleRepository.count() < numberOfRiddles)
            throw new EtAuthException("Invalid Quantity");
        return riddleRepository.getRandomRiddles(numberOfRiddles);
    }

    public Riddle getRiddleById(UUID id) throws EtResourceNotFoundException {
        return riddleRepository.findById(id).orElseThrow(() -> new EtResourceNotFoundException("Riddle " + id + " not found"));
    }

    public Riddle createRiddle(Riddle riddle) throws EtAuthException {
        if (riddleRepository.findRiddleByRiddle(riddle.getRiddle()) != null)
            throw new EtAuthException("Riddle already exist");
        return riddleRepository.save(riddle);
    }

    public Riddle updateRiddle(UUID id, Riddle riddleDetails) throws EtResourceNotFoundException {
        Riddle riddle = riddleRepository.findById(id).orElseThrow(() -> new EtResourceNotFoundException("Riddle " + id + " not found"));
        riddle.copyRiddle(riddleDetails);
        return riddleRepository.save(riddle);
    }

    public boolean deleteRiddle(UUID id) throws EtResourceNotFoundException {
        Riddle riddle = riddleRepository.findById(id)
                .orElseThrow(() -> new EtResourceNotFoundException("Riddle " + id + " not found"));
        riddleRepository.delete(riddle);
        return true;
    }

    public List<RiddleDto> getRiddleDtos(int numberOfDtos) throws EtResourceNotFoundException {
        if (riddleRepository.count() < numberOfDtos || coordinatesService.getCoordinatesRepoCount() < numberOfDtos)
            throw new EtResourceNotFoundException("Resources not found");
        List<Riddle> riddleList = getAListOfRandomRiddles(numberOfDtos);
        List<Coordinates> coordinatesList = coordinatesService.getRandomCoordinates(numberOfDtos);
        List<RiddleDto> riddleDtoList = new ArrayList<>();
        for(int i=0 ;i<numberOfDtos;i++){
            riddleDtoList.add(riddleList.get(i).convertToRiddleDto(coordinatesList.get(i)));
        }
        return riddleDtoList;
    }

}
