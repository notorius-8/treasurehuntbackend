package com.notorious8.treasurehunt.model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "coordinates")
@EntityListeners(AuditingEntityListener.class)
public class Coordinates {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID coordinatesId;

    @Column(name = "longitude", nullable = false)
    private double longitude;

    @Column(name = "latitude", nullable = false)
    private double latitude;

    public Coordinates() {
    }

    public Coordinates(UUID coordinatesId, double longitude, double latitude) {
        this.coordinatesId = coordinatesId;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public UUID getCoordinatesId() {
        return coordinatesId;
    }

    public void copyCoordinates(Coordinates coordinatesDetails) {
        setLatitude(coordinatesDetails.getLatitude());
        setLongitude(coordinatesDetails.getLongitude());
    }
}
