package com.notorious8.treasurehunt.model;

import com.notorious8.treasurehunt.dto.PlayerGameInfoDto;
import com.notorious8.treasurehunt.dto.PlayerLoginDto;
import com.notorious8.treasurehunt.dto.PlayerProgressInfoDto;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "players")
@EntityListeners(AuditingEntityListener.class)
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID playerId;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "surname", nullable = false)
    private String surname;

    @Column(name = "user_name", nullable = false)
    private String userName;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "password", nullable = false)
    private String password;


    public Player() {
    }

    public Player(UUID playerId, String name, String surname, String userName, String email, String password) {
        this.playerId = playerId;
        this.userName = userName;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UUID getPlayerId() {
        return playerId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void copyPlayer(Player playerDetails) {
        setName(playerDetails.getName());
        setUserName(playerDetails.getUserName());
        setSurname(playerDetails.getSurname());
        setEmail(playerDetails.getEmail());
        setPassword(playerDetails.getPassword());
    }

    public PlayerGameInfoDto convertToPlayerGameInfoDto() {
        PlayerGameInfoDto playerGameInfoDto = new PlayerGameInfoDto();
        playerGameInfoDto.setPlayerId(getPlayerId());
        playerGameInfoDto.setUserName(getUserName());
        playerGameInfoDto.setPoints(0);
        return playerGameInfoDto;
    }

    public PlayerLoginDto convertToPlayerLoginDto() {
        PlayerLoginDto playerLoginDto = new PlayerLoginDto();
        playerLoginDto.setPlayerId(getPlayerId());
        playerLoginDto.setUserName(getUserName());
        return playerLoginDto;
    }

}
