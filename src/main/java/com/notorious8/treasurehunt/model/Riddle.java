package com.notorious8.treasurehunt.model;


import com.notorious8.treasurehunt.dto.RiddleDto;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "riddles")
@EntityListeners(AuditingEntityListener.class)
public class Riddle {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID riddleId;

    @Column(name = "riddle", nullable = false)
    private String riddle;

    @Column(name = "correct_answer", nullable = false)
    private String correctAnswer;

    @Column(name = "answer2", nullable = false)
    private String answer2;

    @Column(name = "answer3", nullable = false)
    private String answer3;

    @Column(name = "answer4", nullable = false)
    private String answer4;

    @Column(name = "points", nullable = false)
    private int points;


    public Riddle() {
    }

    public Riddle(UUID riddleId, String riddle, String correctAnswer, String answer2, String answer3, String answer4, int points) {
        this.riddleId = riddleId;
        this.riddle = riddle;
        this.correctAnswer = correctAnswer;
        this.answer2 = answer2;
        this.answer3 = answer3;
        this.answer4 = answer4;
        this.points = points;
    }

    public String getRiddle() {
        return riddle;
    }

    public void setRiddle(String riddle) {
        this.riddle = riddle;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String answer) {
        this.correctAnswer = answer;
    }

    public String getAnswer2() {
        return answer2;
    }

    public void setAnswer2(String answer2) {
        this.answer2 = answer2;
    }

    public String getAnswer3() {
        return answer3;
    }

    public void setAnswer3(String answer3) {
        this.answer3 = answer3;
    }

    public String getAnswer4() {
        return answer4;
    }

    public void setAnswer4(String answer4) {
        this.answer4 = answer4;
    }

    public UUID getRiddleId() {
        return riddleId;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public void copyRiddle(Riddle riddleDetails) {
        setRiddle(riddleDetails.getRiddle());
        setCorrectAnswer(riddleDetails.getCorrectAnswer());
        setAnswer2(riddleDetails.getAnswer2());
        setAnswer3(riddleDetails.getAnswer3());
        setAnswer4(riddleDetails.getAnswer4());
        setPoints(riddleDetails.getPoints());
    }

    public RiddleDto convertToRiddleDto(Coordinates coordinates) {
        RiddleDto riddleDto = new RiddleDto();
        riddleDto.setRiddle(getRiddle());
        riddleDto.setCorrectAnswer(getCorrectAnswer());
        riddleDto.setAnswer2(getAnswer2());
        riddleDto.setAnswer3(getAnswer3());
        riddleDto.setAnswer4(getAnswer4());
        riddleDto.setPoints(getPoints());
        riddleDto.setLatitude(coordinates.getLatitude());
        riddleDto.setLongitude(coordinates.getLongitude());
        return riddleDto;
    }
}
