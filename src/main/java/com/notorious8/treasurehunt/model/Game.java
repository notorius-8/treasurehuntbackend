package com.notorious8.treasurehunt.model;

import com.notorious8.treasurehunt.dto.*;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Game {
    private final String id;
    private List<PlayerGameInfoDto> playerList;
    private List<RiddleDto> riddleList;
    private int scoreToWin;
    private String winner;
    private boolean finished;


    public Game() {
        id = RandomStringUtils.randomAlphabetic(5);
        playerList = new ArrayList<>();
        riddleList = new ArrayList<>();
        scoreToWin = 0;
        winner = null;
        finished = false;
    }

    public String getId() {
        return id;
    }

    public List<PlayerGameInfoDto> getPlayerList() {
        return playerList;
    }

    public void setPlayerList(List<PlayerGameInfoDto> playerList) {
        this.playerList = playerList;
    }

    public List<RiddleDto> getRiddleList() {
        return riddleList;
    }

    public void setRiddleList(List<RiddleDto> riddleList) {
        this.riddleList = riddleList;
    }


    public boolean addPlayer(PlayerGameInfoDto player) {
        if (this.playerList.stream().filter(playerGameInfoDto -> player.getPlayerId().equals(playerGameInfoDto.getPlayerId())).findAny().orElse(null) == null) {
            return playerList.add(player);
        }
        return false;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public boolean isFinished() {
        return finished;
    }

    public int getScoreToWin() {
        return scoreToWin;
    }

    public void setScoreToWin(int scoreToWin) {
        this.scoreToWin = scoreToWin;
    }

    public int addPointsToPlayer(UUID playerId, int points) throws Exception {
        return playerList.stream().filter(player -> playerId.equals(player.getPlayerId()))
                .findAny()
                .orElseThrow(() -> new Exception("Player " + playerId + " not found"))
                .addPoints(points);
    }

    public GameInfoDto convertToGameInfoDto() {
        GameInfoDto gameInfoDto = new GameInfoDto();
        gameInfoDto.setGameId(getId());
        gameInfoDto.setNumberOfPlayers(getPlayerList().size());
        gameInfoDto.setNumberOfRiddles(getRiddleList().size());
        return gameInfoDto;
    }

    public CreateGameDto convertToCreateGameDto() {
        CreateGameDto createGameDto = new CreateGameDto();
        createGameDto.setGameId(getId());
        createGameDto.setRiddleDtoList(getRiddleList());
        createGameDto.setScoreToWin(getScoreToWin());
        return createGameDto;
    }

    public GameStatusDto convertToGameStatusDto() {
        GameStatusDto gameStatusDto = new GameStatusDto();
        gameStatusDto.setWinner(getWinner());
        gameStatusDto.setFinished(isFinished());
        return gameStatusDto;
    }

    public JoinGameDto convertToJoinGameDto() {
        JoinGameDto joinGameDto = new JoinGameDto();
        joinGameDto.setRiddleDtoList(getRiddleList());
        joinGameDto.setScoreToWin(getScoreToWin());
        return joinGameDto;
    }
}
