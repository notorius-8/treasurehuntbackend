package com.notorious8.treasurehunt.repositories;

import com.notorious8.treasurehunt.exceptions.EtAuthException;
import com.notorious8.treasurehunt.model.Riddle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface RiddleRepository extends JpaRepository<Riddle, UUID> {


    @Query(value = "SELECT * FROM riddles ORDER BY RANDOM() LIMIT ?",nativeQuery = true)
    List<Riddle> getRandomRiddles(int quantity) throws EtAuthException;

    Riddle findRiddleByRiddle( String riddle) throws EtAuthException;

}
