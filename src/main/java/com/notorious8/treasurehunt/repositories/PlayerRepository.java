package com.notorious8.treasurehunt.repositories;

import com.notorious8.treasurehunt.model.Player;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PlayerRepository extends JpaRepository<Player, UUID> {

    Player findByEmailAndPassword(String email,String password);
    Player findByEmail(String email);
}
