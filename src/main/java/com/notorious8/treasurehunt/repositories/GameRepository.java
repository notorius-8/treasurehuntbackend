package com.notorious8.treasurehunt.repositories;

import com.notorious8.treasurehunt.model.Game;

import java.util.List;


public interface GameRepository {

    List<Game> getAllGames();

    Game findGameById(String id);

    boolean addGame(Game game);

    boolean deleteGame(String id);

    Game createGame();

}
