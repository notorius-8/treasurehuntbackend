package com.notorious8.treasurehunt.repositories;

import com.notorious8.treasurehunt.model.Game;

import com.notorious8.treasurehunt.services.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository("fakeDao")
public class GameAccessService implements GameRepository {

    private final List<Game> games;

    private final PlayerService playerService;

    @Autowired
    public GameAccessService(PlayerService playerService) {
        this.playerService = playerService;
        games = new ArrayList<>();
    }


    @Override
    public List<Game> getAllGames() {
        return games;
    }

    @Override
    public Game findGameById(String id) {
        return games.stream().filter(game -> id.equals(game.getId()))
                .findAny()
                .orElse(null);
    }

    @Override
    public boolean addGame(Game game) {
        return games.add(game);
    }

    @Override
    public boolean deleteGame(String id) {
        return games.remove(findGameById(id));
    }

    @Override
    public Game createGame() {
        return new Game();
    }


}
