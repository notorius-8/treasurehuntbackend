package com.notorious8.treasurehunt.repositories;

import com.notorious8.treasurehunt.exceptions.EtAuthException;
import com.notorious8.treasurehunt.model.Coordinates;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;


public interface CoordinatesRepository extends JpaRepository<Coordinates, UUID> {

    @Query(value = "SELECT * FROM coordinates ORDER BY RANDOM() LIMIT ?",nativeQuery = true)
    List<Coordinates> getRandomCoordinates(int quantity) throws EtAuthException;

    Coordinates findByLongitudeAndLatitude(Double longitude, Double latitude) throws EtAuthException;

}
