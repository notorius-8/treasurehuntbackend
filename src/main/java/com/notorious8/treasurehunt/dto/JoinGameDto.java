package com.notorious8.treasurehunt.dto;

import java.util.List;

public class JoinGameDto {
    private List<RiddleDto> riddleDtoList;
    private int scoreToWin;

    public JoinGameDto(List<RiddleDto> riddleDtoList, int scoreToWin) {
        this.riddleDtoList = riddleDtoList;
        this.scoreToWin = scoreToWin;
    }

    public JoinGameDto() {
    }

    public int getScoreToWin() {
        return scoreToWin;
    }

    public void setScoreToWin(int scoreToWin) {
        this.scoreToWin = scoreToWin;
    }

    public List<RiddleDto> getRiddleDtoList() {
        return riddleDtoList;
    }

    public void setRiddleDtoList(List<RiddleDto> riddleDtoList) {
        this.riddleDtoList = riddleDtoList;
    }
}

