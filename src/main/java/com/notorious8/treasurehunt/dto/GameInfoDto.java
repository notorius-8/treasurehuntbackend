package com.notorious8.treasurehunt.dto;

public class GameInfoDto {
    private String gameId;
    private int numberOfPlayers;
    private int numberOfRiddles;

    public GameInfoDto() {
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public int getNumberOfPlayers() {
        return numberOfPlayers;
    }

    public void setNumberOfPlayers(int numberOfPlayers) {
        this.numberOfPlayers = numberOfPlayers;
    }

    public int getNumberOfRiddles() {
        return numberOfRiddles;
    }

    public void setNumberOfRiddles(int numberOfRiddles) {
        this.numberOfRiddles = numberOfRiddles;
    }
}
