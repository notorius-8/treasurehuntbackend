package com.notorious8.treasurehunt.dto;

import java.util.UUID;

public class PlayerGameInfoDto {
    private UUID playerId;
    private String userName;
    private int points;

    public PlayerGameInfoDto(UUID playerId, String userName, int points) {
        this.playerId = playerId;
        this.userName = userName;
        this.points = points;
    }

    public PlayerGameInfoDto() {

    }

    public UUID getPlayerId() {
        return playerId;
    }

    public void setPlayerId(UUID playerId) {
        this.playerId = playerId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int addPoints(int points){
        return this.points+=points;
    }


    public PlayerProgressInfoDto convertToPlayerProgressInfoDto() {
        PlayerProgressInfoDto playerProgressInfoDto = new PlayerProgressInfoDto();
        playerProgressInfoDto.setUserName(getUserName());
        playerProgressInfoDto.setScore(getPoints());
        return playerProgressInfoDto;
    }
}
