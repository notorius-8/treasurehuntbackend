package com.notorious8.treasurehunt.dto;

import java.util.UUID;

public class PlayerLoginDto {

    private UUID playerId;
    private String userName;

    public PlayerLoginDto(UUID playerId, String userName) {
        this.playerId = playerId;
        this.userName = userName;
    }

    public PlayerLoginDto() {
    }

    public UUID getPlayerId() {
        return playerId;
    }

    public void setPlayerId(UUID playerId) {
        this.playerId = playerId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
