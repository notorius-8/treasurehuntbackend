package com.notorious8.treasurehunt.controller;

import com.notorious8.treasurehunt.dto.RiddleDto;
import com.notorious8.treasurehunt.exceptions.EtResourceNotFoundException;
import com.notorious8.treasurehunt.model.Riddle;
import com.notorious8.treasurehunt.services.RiddleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RequestMapping("/api/v1")
@RestController
public class RiddleController {


    private final RiddleService riddleService;

    @Autowired
    public RiddleController(RiddleService riddleService)
    {
        this.riddleService = riddleService;
    }

    @GetMapping("/riddles")
    @ResponseBody
    public List<Riddle> getAllRiddles() {
        return riddleService.getAListOfAllRiddles();
    }

    @GetMapping("/riddles/{id}")
    @ResponseBody
    public ResponseEntity<Riddle> getRiddleById(@PathVariable(value = "id") UUID id) throws EtResourceNotFoundException {
        return ResponseEntity.ok(riddleService.getRiddleById(id));
    }

    @GetMapping("/riddles/random/{numberOfRiddles}")
    public List<Riddle> getRandomRiddles(@PathVariable(value = "numberOfRiddles") int numberOfRiddles) throws EtResourceNotFoundException {
        return riddleService.getAListOfRandomRiddles(numberOfRiddles);
    }

    @PostMapping("/riddles")
    @ResponseBody
    public ResponseEntity<Riddle> createRiddle(@RequestBody Riddle riddle) {
        return ResponseEntity.ok(riddleService.createRiddle(riddle));
    }

    @PutMapping("/riddles/{id}")
    @ResponseBody
    public ResponseEntity<Riddle> updateRiddle(@PathVariable(value = "id") UUID id, @RequestBody Riddle riddleDetails) throws EtResourceNotFoundException {
        return ResponseEntity.ok(riddleService.updateRiddle(id,riddleDetails));
    }

    @DeleteMapping("/riddles/{id}")
    public boolean deleteRiddle(@PathVariable(value = "id") UUID id) throws EtResourceNotFoundException {
        return riddleService.deleteRiddle(id);
    }

    @GetMapping("/riddleDto/random/{numberOfRiddleDtos}")
    @ResponseBody
    public List<RiddleDto> getRiddleDtos(@PathVariable(value = "numberOfRiddleDtos") int numberOfDtos) {
        return riddleService.getRiddleDtos(numberOfDtos);
    }
}
