package com.notorious8.treasurehunt.controller;

import com.notorious8.treasurehunt.model.Coordinates;
import com.notorious8.treasurehunt.repositories.CoordinatesRepository;
import com.notorious8.treasurehunt.services.CoordinatesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RequestMapping("/api/v1")
@RestController
public class CoordinatesController {

    @Autowired
    private CoordinatesService coordinatesService;

    @GetMapping("/coordinates")
    public List<Coordinates> getAllCoordinates() {
        return coordinatesService.getAListOfAllCoordinates();
    }

    @GetMapping("/coordinates/{id}")
    public ResponseEntity<Coordinates> getCoordinatesById(@PathVariable(value = "id") UUID id){
        return ResponseEntity.ok().body(coordinatesService.getCoordinatesById(id));
    }

    @GetMapping("/coordinates/random/{numberOfCoordinates}")
    public List<Coordinates> getRandomCoordinates(@PathVariable(value = "numberOfCoordinates") int numberOfCoordinates) {
        return coordinatesService.getRandomCoordinates(numberOfCoordinates);
    }

    @PostMapping("/coordinates")
    @ResponseBody
    public ResponseEntity<Coordinates> createCoordinates(@RequestBody Coordinates coordinates) {
        return ResponseEntity.ok(coordinatesService.createCoordinates(coordinates));
    }

    @PutMapping("/coordinates/{id}")
    @ResponseBody
    public ResponseEntity<Coordinates> updateCoordinates(@PathVariable(value = "id") UUID id, @RequestBody Coordinates coordinatesDetails){
        return ResponseEntity.ok(coordinatesService.updateCoordinates(id, coordinatesDetails));
    }

    @DeleteMapping("/coordinates/{id}")
    public boolean deleteCoordinates(@PathVariable(value = "id") UUID id){
        return coordinatesService.deleteCoordinates(id);
    }
}
