package com.notorious8.treasurehunt.controller;


import com.notorious8.treasurehunt.dto.*;
import com.notorious8.treasurehunt.services.GameService;
import com.notorious8.treasurehunt.model.Game;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RequestMapping("/api/v1")
@RestController
public class GameController {

    @Autowired
    GameService gameService;

    @GetMapping("/getGames")
    public List<GameInfoDto> getAllGames() {
        return gameService.getAllGames();
    }

    @PostMapping("/createGame/{numberOfRiddles}/{playerId}")
    public ResponseEntity<CreateGameDto> createGame(@PathVariable(value = "numberOfRiddles") int numberOfRiddles, @PathVariable(value = "playerId") UUID playerId) throws Exception {
        return ResponseEntity.ok(gameService.createGame(numberOfRiddles,playerId));
    }

    @GetMapping("/findGame/{id}")
    public Game findGameById(@PathVariable(value = "id") String id) {
        return gameService.findGameById(id);
    }

    @PostMapping("/joinGame/{gameId}/{playerId}")
    public JoinGameDto joinGame(@PathVariable(value = "gameId") String gameId, @PathVariable(value = "playerId") UUID playerId) throws Exception {
        return gameService.joinGame(gameId, playerId);
    }

    @DeleteMapping("/deleteGame/{id}")
    public boolean deleteGame(@PathVariable(value = "id") String id) {
        return gameService.deleteGame(id);
    }

    @GetMapping("/getGameRiddles/{id}")
    public List<RiddleDto> getGameRiddles(@PathVariable(value = "id") String id) {
        return gameService.getGameRiddles(id);
    }

    @PostMapping("/addPointsToPlayer/{gameId}/{playerId}/{points}")
    @ResponseBody
    public int addPointsToPlayer(@PathVariable(value = "gameId") String gameId, @PathVariable(value = "playerId") UUID playerId, @PathVariable(value = "points") int points) throws Exception {
        return gameService.addPointsToPlayer(gameId, playerId, points);

    }
    @GetMapping("/getPlayersInGameInfo/{gameId}")
    public List<PlayerProgressInfoDto> getPlayersInGameInfo(@PathVariable(value = "gameId") String gameId) throws Exception {
        return gameService.getPlayersInGameInfo(gameId);
    }

    @GetMapping("/getGameStatus/{id}")
    public GameStatusDto getGameStatus(@PathVariable(value = "id") String id) {
        return gameService.getGameStatus(id);
    }
}
