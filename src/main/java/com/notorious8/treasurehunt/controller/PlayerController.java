package com.notorious8.treasurehunt.controller;

import com.notorious8.treasurehunt.dto.PlayerLoginDto;
import com.notorious8.treasurehunt.model.Player;
import com.notorious8.treasurehunt.services.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RequestMapping("/api/v1")
@RestController
public class PlayerController {

    private final PlayerService playerService;

    @Autowired
    public PlayerController(PlayerService playerService)
    {
        this.playerService = playerService;
    }

    @GetMapping("/players")
    public List<Player> getAllPlayers() throws Exception {
        return playerService.getAllPlayersFromRepository();
    }

    @GetMapping("/players/{id}")
    public ResponseEntity<Player> getPlayerById(@PathVariable(value = "id") UUID id) throws Exception {
        return ResponseEntity.ok().body(playerService.getPlayerById(id));
    }

    @GetMapping("/players/{email}/{password}")
    public PlayerLoginDto getPlayerByEmailAndPassword(@PathVariable String email, @PathVariable String password) throws Exception {
        return playerService.findByEmailAndPassword(email,password);
    }

    @PostMapping("/players")
    public boolean createPlayer(@RequestBody Player player) throws Exception {
        return playerService.createPlayer(player);
    }

    @PutMapping("/players/{id}")
    public ResponseEntity<Player> updatePlayer(@PathVariable(value = "id") UUID id, @RequestBody Player playerDetails) throws Exception {
       return ResponseEntity.ok().body(playerService.updatePlayer(id,playerDetails));
    }

    @DeleteMapping("/players/{id}")
    public boolean deletePlayer(@PathVariable(value = "id") UUID id) throws Exception {
        return playerService.deletePlayer(id);
    }
}
