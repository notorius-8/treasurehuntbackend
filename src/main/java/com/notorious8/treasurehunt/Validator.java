package com.notorious8.treasurehunt;

import com.notorious8.treasurehunt.model.Player;

public class Validator {

    public boolean checkIfPasswordIsStrong(String pass) {
        return pass.length() >= 6;
    }

    public boolean checkIfNull(String text) {
        return text.isEmpty();
    }

    public boolean checkIfEmailIsValid(String email) {
        String expression = "^[a-zA-Z0-9._-]+@[a-z0-9.-]+\\.[a-z]{2,6}$";
        return email.matches(expression);
    }

    public boolean checkPlayerValidation(Player player) throws Exception {

        if (checkIfNull(player.getName())) throw new NullPointerException("Name is null");

        if (checkIfNull(player.getSurname())) throw new NullPointerException("Surname is null");

        if (checkIfNull(player.getUserName())) throw new NullPointerException("Username is null");

        if (checkIfNull(player.getPassword())) throw new NullPointerException("Password is null");

        if (checkIfNull(player.getEmail())) throw new NullPointerException("Email is null");

        if (!checkIfPasswordIsStrong(player.getPassword())) throw new Exception("Weak password");

        if (!checkIfEmailIsValid(player.getEmail())) throw new Exception("Invalid email");

        return true;
    }


}