package com.notorious8.treasurehunt.dto;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;

import java.util.UUID;

import static org.junit.Assert.*;

public class PlayerGameInfoDtoTest {
    private PlayerGameInfoDto playerGameInfoDto;

    @Before
    public void setUp() throws Exception {
        playerGameInfoDto = new PlayerGameInfoDto();
        playerGameInfoDto.setPlayerId(UUID.randomUUID());
        playerGameInfoDto.setUserName("kkouts");
        playerGameInfoDto.setPoints(0);
    }

    @After
    public void tearDown() throws Exception {
        playerGameInfoDto = null;
    }

    @Test
    public void testConvertToPlayerProgressInfoDto() {
        PlayerProgressInfoDto playerProgressInfoDto = new PlayerProgressInfoDto();
        playerProgressInfoDto.setUserName(playerGameInfoDto.getUserName());
        playerProgressInfoDto.setScore(playerGameInfoDto.getPoints());
        assertTrue(new ReflectionEquals(playerProgressInfoDto).matches(playerGameInfoDto.convertToPlayerProgressInfoDto()));
    }
}