package com.notorious8.treasurehunt;

import com.notorious8.treasurehunt.model.Player;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.*;

public class ValidatorTest {
    private Validator validator;

    @Before
    public void setUp() {
        validator = new Validator();
    }

    @After
    public void tearDown() {
        validator = null;
    }

    @Test
    public void testCheckIfPasswordIsStrongWhenPasswordSizeLessThanSix() {
        assertFalse(validator.checkIfPasswordIsStrong("12345"));
    }

    @Test
    public void testCheckIfPasswordIsStrongWhenPasswordSizeGreaterThanSix() {
        assertTrue(validator.checkIfPasswordIsStrong("testPass12345"));
    }

    @Test
    public void testCheckIfPasswordIsStrongWhenPasswordSizeEqualsSix() {
        ;
        assertTrue(validator.checkIfPasswordIsStrong("test12"));
    }

    @Test
    public void testCheckIfNullWhenGivenNull() {
        assertTrue(validator.checkIfNull(""));
    }

    @Test
    public void testCheckIfNullWhenGivenNotNull() {
        assertFalse(validator.checkIfNull("test"));
    }

    @Test
    public void testCheckIfEmailIsValidWhenValidEmailGiver() {
        assertTrue(validator.checkIfEmailIsValid("kostas@gmail.com"));
    }

    @Test
    public void testCheckIfEmailIsValidWhenInvalidEmailGiver() {
        assertFalse(validator.checkIfEmailIsValid("ko$ho.af"));
    }

    @Test(expected = NullPointerException.class)
    public void testCheckValidationWhenNullNameGiven() throws Exception {
        validator.checkPlayerValidation(new Player(UUID.randomUUID(),"", "Koutsilis", "kkouts", "kkouts@gmail.com", "testpass1234"));
        fail("NullPointerException not thrown at testCheckValidationWhenNullNameGiven");
    }

    @Test(expected = NullPointerException.class)
    public void testCheckValidationWhenNullSurnameGiven() throws Exception {
        validator.checkPlayerValidation(new Player(UUID.randomUUID(),"Konstantinos", "", "kkouts", "kkouts@gmail.com", "testpass1234"));
        fail("NullPointerException not thrown at testCheckValidationWhenNullSurnameGiven");
    }

    @Test(expected = NullPointerException.class)
    public void testCheckValidationWhenNullUsernameGiven() throws Exception {
        validator.checkPlayerValidation(new Player(UUID.randomUUID(),"Konstantinos", "Koutsilis", "", "kkouts@gmail.com", "testpass1234"));
        fail("NullPointerException not thrown at testCheckValidationWhenNullUsernameGiven");
    }

    @Test(expected = NullPointerException.class)
    public void testCheckValidationWhenNullEmailGiven() throws Exception {
        validator.checkPlayerValidation(new Player(UUID.randomUUID(),"Konstantinos", "Koutsilis", "kkouts", "", "testpass1234"));
        fail("NullPointerException not thrown at testCheckValidationWhenNullEmailGiven");
    }

    @Test(expected = NullPointerException.class)
    public void testCheckValidationWhenNullPasswordGiven() throws Exception {
        validator.checkPlayerValidation(new Player(UUID.randomUUID(),"Konstantinos", "Koutsilis", "kkouts", "kostas@gmail.com", ""));
        fail("NullPointerException not thrown at testCheckValidationWhenNullPasswordGiven");
    }

    @Test(expected = Exception.class)
    public void testCheckValidationWhenWeakPasswordGiven() throws Exception {
        validator.checkPlayerValidation(new Player(UUID.randomUUID(),"Konstantinos", "Koutsilis", "kkouts", "kostas@gmail.com", "123"));
        fail("Exception not thrown at testCheckValidationWhenWeakPasswordGiven");
    }

    @Test(expected = Exception.class)
    public void testCheckValidationWhenInvalidEmailGiven() throws Exception {
        validator.checkPlayerValidation(new Player(UUID.randomUUID(),"Konstantinos", "Koutsilis", "kkouts", "@kostascom", "testpass123"));
        fail("Exception not thrown at testCheckValidationWhenInvalidEmailGiven");
    }

    @Test
    public void testCheckValidationWhenEverythingIsCorrect() throws Exception {
        assertTrue(validator.checkPlayerValidation(new Player(UUID.randomUUID(),"Konstantinos", "Koutsilis", "kkouts", "kostas@gmail.com", "testpass123")));
    }
}