package com.notorious8.treasurehunt.repositories;

import com.notorious8.treasurehunt.model.Game;
import com.notorious8.treasurehunt.services.PlayerService;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class GameAccessServiceTest {

    @InjectMocks
    private GameAccessService gameAccessService;

    private Game game;

    @Before
    public void setUp() {
        game = new Game();
    }

    @After
    public void tearDown() {
        game = null;
    }

    @Test
    public void testFindGameByIdWhenThereIsAGame() {
        gameAccessService.addGame(game);
        assertEquals(game.getId(), gameAccessService.findGameById(game.getId()).getId());
    }
    @Test
    public void testFindGameByIdWhenThereIsNoGame() {
        assertNull(gameAccessService.findGameById(game.getId()));
    }

    @Test
    public void testAddGame() {
        assertTrue(gameAccessService.addGame(game));
    }

    @Test
    public void testDeleteGameWhenThereIsAGameToDelete() {
        gameAccessService.addGame(game);
        assertTrue(gameAccessService.deleteGame(game.getId()));
    }
    @Test
    public void testDeleteGameWhenThereIsNoGameToDelete() {
        assertFalse(gameAccessService.deleteGame(game.getId()));
    }

    @Test
    public void testCreateGame() {
        assertTrue(gameAccessService.createGame() instanceof Game);
    }

    @Test
    public void testGetAllGamesWhenThereAreGames() {
        Game anotherGame = new Game();
        gameAccessService.addGame(game);
        gameAccessService.addGame(anotherGame);
        assertEquals(Arrays.asList(game,anotherGame), gameAccessService.getAllGames());
    }
    @Test
    public void testGetAllGamesWhenThereAreNoGames() {
        assertTrue(gameAccessService.getAllGames().isEmpty());
    }
}
