package com.notorious8.treasurehunt.repositories;

import com.notorious8.treasurehunt.model.Player;
import com.notorious8.treasurehunt.model.Riddle;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class PlayerRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private PlayerRepository playerRepository;

    private Player player;

    @Before
    public void setUp() {
        player = new Player(UUID.randomUUID(), "Kostas", "Kouts", "SpodX","kouts@gmail.com", "1234567f");
    }

    @Test
    public void testFindByEmailAndPassword() {
        entityManager.merge(player);
        assertEquals(player.getEmail(), playerRepository.findByEmailAndPassword(player.getEmail(), player.getPassword()).getEmail());
        assertEquals(player.getPassword(), playerRepository.findByEmailAndPassword(player.getEmail(), player.getPassword()).getPassword());
    }

    @Test
    public void testFindByEmail() {
        entityManager.merge(player);
        assertEquals(player.getEmail(), playerRepository.findByEmail(player.getEmail()).getEmail());
    }
}