package com.notorious8.treasurehunt.repositories;

import com.notorious8.treasurehunt.model.Riddle;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class RiddleRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private RiddleRepository riddleRepository;

//    @Test
//    public void testGetRandomRiddles() {
//        List<Riddle> riddleList = new ArrayList<>();
//        riddleList.add(new Riddle(UUID.randomUUID(), "number", "1", "a", "b", "c", 10));
//        riddleList.add(new Riddle(UUID.randomUUID(), "word", "a", "1", "2", "3", 10));
//
//        entityManager.merge(riddleList.get(0));
//        entityManager.merge(riddleList.get(1));
//
//        assertEquals(riddleList.get(0).getRiddle(), riddleRepository.findRiddleByRiddle(riddleRepository.getRandomRiddles(2).get(0).getRiddle()).getRiddle());
//        assertEquals(riddleList.get(0).getRiddle(), riddleRepository.getRandomRiddles(2).get(1).getRiddle());
//
//    }

    @Test
    public void findRiddleByRiddle() {
        Riddle riddle = new Riddle(UUID.randomUUID(), "number", "1", "a", "b", "c", 10);
        entityManager.merge(riddle);
        assertEquals(riddle.getRiddle(), riddleRepository.findRiddleByRiddle(riddle.getRiddle()).getRiddle());
    }
}