package com.notorious8.treasurehunt.repositories;

import com.notorious8.treasurehunt.model.Coordinates;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class CoordinatesRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private CoordinatesRepository coordinatesRepository;

    @Test
    public void findByLongitudeAndLatitude() {
        Coordinates coordinates = new Coordinates(UUID.randomUUID(),41.0752080782,23.5543570314);
        entityManager.merge(coordinates);
        assertEquals(coordinates.getLongitude(),coordinatesRepository.findByLongitudeAndLatitude(coordinates.getLongitude(),coordinates.getLatitude()).getLongitude(),0.0000000001);
    }
}