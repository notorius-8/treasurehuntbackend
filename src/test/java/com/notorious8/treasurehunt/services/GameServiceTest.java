package com.notorious8.treasurehunt.services;

import com.notorious8.treasurehunt.dto.*;
import com.notorious8.treasurehunt.model.Game;
import com.notorious8.treasurehunt.model.Player;
import com.notorious8.treasurehunt.repositories.GameRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GameServiceTest {

    @InjectMocks
    private GameService gameService;

    @Mock
    private GameRepository gameRepository;

    @Mock
    private RiddleService riddleService;

    @Mock
    private PlayerService playerService;

    private Game firstGame;
    private List<Game> gameList;
    private List<GameInfoDto> gameInfoDtoList;
    private List<RiddleDto> riddleDtoList;
    private Player player;
    private GameStatusDto gameStatusDto;
    private int points;

    @Before
    public void setUp() {
        firstGame = new Game();
        player = new Player(UUID.randomUUID(), "Jason", "Mpouzgos", "Json", "jason12@gmail.com", "1234567f");
        gameStatusDto = firstGame.convertToGameStatusDto();
        riddleDtoList = Arrays.asList(new RiddleDto(), new RiddleDto(), new RiddleDto(), new RiddleDto(), new RiddleDto());
        firstGame.addPlayer(player.convertToPlayerGameInfoDto());
        ;
        gameList = Arrays.asList(new Game(), new Game());
        gameInfoDtoList = Arrays.asList(gameList.get(0).convertToGameInfoDto(), gameList.get(1).convertToGameInfoDto());
        points = 10;
    }

    @After
    public void tearDown() {
        firstGame = null;
        player = null;
        gameStatusDto = null;
        riddleDtoList = null;
        gameList = null;
        gameInfoDtoList = null;
    }

    @Test
    public void testGetAllGamesWhenThereAreGames() {
        when(gameRepository.getAllGames()).thenReturn(gameList);
        assertTrue(new ReflectionEquals(gameInfoDtoList.toArray()).matches(gameService.getAllGames().toArray()));
    }

    @Test
    public void testGetAllGamesWhenThereAreNoGames() {
        assertEquals(Collections.emptyList(), gameService.getAllGames());
    }

    @Test
    public void testFindGameByIdWhenThereIsAGame() {
        when(gameRepository.findGameById(firstGame.getId())).thenReturn(firstGame);
        assertEquals(firstGame, gameService.findGameById(firstGame.getId()));

    }

    @Test
    public void testFindGameByIdWhenThereIsNoGame() {
        assertNull(gameService.findGameById(firstGame.getId()));

    }

    @Test
    public void testCreateGame() throws Exception {
        CreateGameDto createGameDto = new CreateGameDto();
        createGameDto.setGameId(firstGame.getId());
        createGameDto.setRiddleDtoList(riddleDtoList);
        when(gameRepository.createGame()).thenReturn(firstGame);
        when(riddleService.getRiddleDtos(5)).thenReturn(riddleDtoList);
        when(playerService.getPlayerById(player.getPlayerId())).thenReturn(player);
        assertTrue(new ReflectionEquals(gameService.createGame(5, player.getPlayerId())).matches(createGameDto));
    }

    @Test(expected = Exception.class)
    public void testCreateGameWhenThereIsNoPlayer() throws Exception {
        CreateGameDto createGameDto = new CreateGameDto();
        createGameDto.setGameId(firstGame.getId());
        createGameDto.setRiddleDtoList(riddleDtoList);
        when(gameRepository.createGame()).thenReturn(firstGame);
        when(riddleService.getRiddleDtos(5)).thenReturn(riddleDtoList);
        gameService.createGame(5, player.getPlayerId());
        fail("Exception not thrown in testCreateGameWhenThereIsNoPlayer");
    }

    @Test
    public void testJoinGame() throws Exception {
        Game secondGame = new Game();
        secondGame.setRiddleList(riddleDtoList);
        when(gameRepository.findGameById(secondGame.getId())).thenReturn(secondGame);
        when(playerService.getPlayerById(player.getPlayerId())).thenReturn(player);
        assertTrue(new ReflectionEquals(gameService.joinGame(secondGame.getId(), player.getPlayerId()).getRiddleDtoList()).matches(riddleDtoList));
    }

    @Test
    public void testJoinGameWhenPlayerAlreadyJoined() throws Exception {
        when(gameRepository.findGameById(firstGame.getId())).thenReturn(firstGame);
        when(playerService.getPlayerById(player.getPlayerId())).thenReturn(player);
        gameService.joinGame(firstGame.getId(),player.getPlayerId());
        assertEquals(1,firstGame.getPlayerList().size());
    }

    @Test(expected = Exception.class)
    public void testJoinGameExceptionThrownIfGameDoesNotExist() throws Exception {
        Game secondGame = new Game();
        secondGame.setRiddleList(riddleDtoList);
        gameService.joinGame(secondGame.getId(), player.getPlayerId());
        fail("Exception not thrown in estJoinGameExceptionThrownIfGameDoesNotExist");
    }

    @Test(expected = Exception.class)
    public void testJoinGameExceptionThrownIfPlayerDoesNotExist() throws Exception {
        Game secondGame = new Game();
        secondGame.setRiddleList(riddleDtoList);
        when(gameRepository.findGameById(secondGame.getId())).thenReturn(secondGame);
        gameService.joinGame(secondGame.getId(), player.getPlayerId());
        fail("Exception not thrown in testJoinGameExceptionThrownIfPlayerDoesNotExist");
    }

    @Test(expected = Exception.class)
    public void testJoinGameWhenGameIsFinished() throws Exception {
        Game secondGame = new Game();
        secondGame.setFinished(true);
        when(gameRepository.findGameById(secondGame.getId())).thenReturn(secondGame);
        when(playerService.getPlayerById(player.getPlayerId())).thenReturn(player);
        gameService.joinGame(secondGame.getId(), player.getPlayerId());
        fail("Exception not thrown in testJoinGameWhenGameIsFinished");
    }

    @Test
    public void testDeleteGame() {
        when(gameRepository.deleteGame(firstGame.getId())).thenReturn(true);
        assertTrue(gameService.deleteGame(firstGame.getId()));
    }

    @Test
    public void testDeleteGameWhenThereIsNoGame() {
        assertFalse(gameService.deleteGame(firstGame.getId()));
    }

    @Test
    public void testGetGameRiddles() {
        firstGame.setRiddleList(riddleDtoList);
        when(gameRepository.findGameById(firstGame.getId())).thenReturn(firstGame);
        assertTrue(new ReflectionEquals(gameService.getGameRiddles(firstGame.getId())).matches(riddleDtoList));
    }

    @Test
    public void testAddPointsToPlayer() throws Exception {
        firstGame.setScoreToWin(30);
        when(gameRepository.findGameById(firstGame.getId())).thenReturn(firstGame);
        assertTrue(new ReflectionEquals(gameService.addPointsToPlayer(firstGame.getId(), player.getPlayerId(), points)).matches(points));
    }

    @Test(expected = Exception.class)
    public void testAddPointsToPlayerThrowExceptionTestForGameNotFound() throws Exception {
        assertEquals(10, gameService.addPointsToPlayer(firstGame.getId(), player.getPlayerId(), 10));
        fail("Exception not thrown in testAddPointsToPlayerThrowExceptionTestForGameNotFound");
    }

    @Test(expected = Exception.class)
    public void testAddPointsToPlayerThrowExceptionWhenGivenNegativePoints() throws Exception {
        when(gameRepository.findGameById(firstGame.getId())).thenReturn(firstGame);
        gameService.addPointsToPlayer(firstGame.getId(), player.getPlayerId(), -5);
        fail("Exception not thrown in testAddPointsToPlayerThrowExceptionTestForNegativePointsNumber");
    }


    @Test
    public void testGetGameStatusWhenGameNotFinished() throws Exception {
        firstGame.setScoreToWin(20);
        when(gameRepository.findGameById(firstGame.getId())).thenReturn(firstGame);
        gameService.addPointsToPlayer(firstGame.getId(), player.getPlayerId(), 10);
        assertFalse(gameService.getGameStatus(firstGame.getId()).isFinished());
        assertNull(gameService.getGameStatus(firstGame.getId()).getWinner());
    }

    @Test
    public void testGetGameStatusWhenPlayerWinsTheGame() throws Exception {
        firstGame.setScoreToWin(10);
        when(gameRepository.findGameById(firstGame.getId())).thenReturn(firstGame);
        when(playerService.getPlayerById(player.getPlayerId())).thenReturn(player);
        gameService.addPointsToPlayer(firstGame.getId(), player.getPlayerId(), 10);
        assertTrue(gameService.getGameStatus(firstGame.getId()).isFinished());
        assertEquals(player.getUserName(), gameService.getGameStatus(firstGame.getId()).getWinner());
    }

    @Test
    public void testGetPlayerInGameInfoWhenGameExists() throws Exception {
        when(gameRepository.findGameById(firstGame.getId())).thenReturn(firstGame);
        List<PlayerProgressInfoDto> playerProgressInfoDtos = new ArrayList<>();
        for (PlayerGameInfoDto playerGameInfoDto : firstGame.getPlayerList()) {
            playerProgressInfoDtos.add(playerGameInfoDto.convertToPlayerProgressInfoDto());
        }
        assertTrue(new ReflectionEquals(playerProgressInfoDtos.toArray()).matches(gameService.getPlayersInGameInfo(firstGame.getId()).toArray()));
    }

    @Test(expected = Exception.class)
    public void testGetPlayerInGameInfoWhenGameDoesNotExists() throws Exception {
        gameService.getPlayersInGameInfo(firstGame.getId());
        fail("Exception not thrown at testGetPlayerInGameInfoWhenGameDoesNotExists");
    }

    @Test
    public void testIfGameIsDeletedAfterAPlayerWins() throws Exception {
        firstGame.setScoreToWin(10);
        when(gameRepository.findGameById(firstGame.getId())).thenReturn(firstGame);
        when(playerService.getPlayerById(player.getPlayerId())).thenReturn(player);
        gameService.addPointsToPlayer(firstGame.getId(), player.getPlayerId(), 20);
        Thread.sleep(31000L);
        assertNull(gameList.stream().filter(game -> firstGame.getId().equals(game.getId()))
                .findAny()
                .orElse(null));
    }
}