package com.notorious8.treasurehunt.services;

import com.notorious8.treasurehunt.exceptions.EtAuthException;
import com.notorious8.treasurehunt.exceptions.EtResourceNotFoundException;
import com.notorious8.treasurehunt.model.Coordinates;
import com.notorious8.treasurehunt.repositories.CoordinatesRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CoordinatesServiceTest {

    @InjectMocks
    private CoordinatesService coordinatesService;

    @Mock
    private CoordinatesRepository coordinatesRepository;

    private Coordinates coordinates;
    private List<Coordinates> coordinatesList;

    @Before
    public void setUp() {
        coordinates = new Coordinates(UUID.randomUUID(), 41.0752080982, 23.5543570414);
        coordinatesList = Arrays.asList(coordinates, coordinates);
    }

    @After
    public void tearDown() {
        coordinates = null;
        coordinatesList = null;
    }

    @Test
    public void testGetAListOfAllCoordinatesWhenThereAreCoordinates() throws EtAuthException {
        when(coordinatesRepository.count()).thenReturn(2L);
        when(coordinatesRepository.findAll()).thenReturn(coordinatesList);
        assertTrue(new ReflectionEquals(coordinatesService.getAListOfAllCoordinates()).matches(coordinatesList));
    }

    @Test(expected = EtAuthException.class)
    public void testGetAListOfAllCoordinatesWhenThereAreNoCoordinates() throws EtAuthException {
        coordinatesService.getAListOfAllCoordinates();
        fail("EtAuthException not thrown at testGetAListOfAllCoordinatesWhenThereAreNoCoordinates");
    }

    @Test
    public void testGetRandomCoordinatesWhenThereAreCoordinates() throws EtAuthException {
        when(coordinatesRepository.count()).thenReturn(3L);
        when(coordinatesRepository.getRandomCoordinates(2)).thenReturn(coordinatesList);
        assertTrue(new ReflectionEquals(coordinatesService.getRandomCoordinates(2)).matches(coordinatesList));
    }

    @Test(expected = EtAuthException.class)
    public void testGetRandomCoordinatesWhenThereAreNoCoordinates() {
        coordinatesService.getRandomCoordinates(2);
        fail("EtAuthException not thrown at testGetRandomCoordinatesWhenThereAreNoCoordinates");
    }

    @Test
    public void testGetCoordinatesByIdWhenExists() throws EtResourceNotFoundException {
        when(coordinatesRepository.findById(coordinates.getCoordinatesId())).thenReturn(java.util.Optional.ofNullable(coordinates));
        assertTrue(new ReflectionEquals(coordinatesService.getCoordinatesById(coordinates.getCoordinatesId())).matches(coordinates));
    }

    @Test(expected = EtResourceNotFoundException.class)
    public void testGetCoordinatesByIdWhenIdDoesNotExists() throws EtResourceNotFoundException {
        coordinatesService.getCoordinatesById(coordinates.getCoordinatesId());
        fail("EtResourceNotFoundException not thrown at testGetCoordinatesByIdWhenIdDoesNotExists");
    }

    @Test
    public void testCreateCoordinatesWhenDoesNotExists() throws EtAuthException {
        when(coordinatesRepository.save(coordinates)).thenReturn(coordinates);
        when(coordinatesRepository.findByLongitudeAndLatitude(coordinates.getLongitude(), coordinates.getLatitude())).thenReturn(null);
        assertEquals(coordinates,coordinatesService.createCoordinates(coordinates));
    }

    @Test(expected = EtAuthException.class)
    public void testCreateCoordinatesWhenAlreadyExists() throws EtAuthException {
        when(coordinatesRepository.findByLongitudeAndLatitude(coordinates.getLongitude(), coordinates.getLatitude())).thenReturn(coordinates);
        coordinatesService.createCoordinates(coordinates);
        fail("EtAuthException not thrown at testCreateCoordinatesWhenAlreadyExists");
    }

    @Test
    public void testUpdateCoordinatesWhenExists() throws EtResourceNotFoundException {
        UUID id = coordinates.getCoordinatesId();
        Coordinates newCoordinates = new Coordinates(coordinates.getCoordinatesId(), 41.0752080985, 23.5543570416);
        when(coordinatesRepository.findById(id)).thenReturn(java.util.Optional.of(coordinates));
        when(coordinatesRepository.save(coordinates)).thenReturn(newCoordinates);
        assertTrue(new ReflectionEquals(coordinatesService.updateCoordinates(id, newCoordinates)).matches(coordinates));
    }

    @Test(expected = EtResourceNotFoundException.class)
    public void testUpdateCoordinatesWhenCoordinatesDoesNotExist() throws EtResourceNotFoundException {
        coordinatesService.updateCoordinates(coordinates.getCoordinatesId(), new Coordinates(coordinates.getCoordinatesId(), coordinates.getLongitude(), coordinates.getLatitude()));
        fail("EtResourceNotFoundException not thrown at testUpdateCoordinatesWhenCoordinatesDoesNotExists");
    }

    @Test
    public void testDeleteCoordinatesWhenCoordinatesExists() throws EtResourceNotFoundException {
        when(coordinatesRepository.findById(coordinates.getCoordinatesId())).thenReturn(Optional.ofNullable(coordinates));
        assertTrue(coordinatesService.deleteCoordinates(coordinates.getCoordinatesId()));
    }

    @Test(expected = EtResourceNotFoundException.class)
    public void testDeleteCoordinatesWhenCoordinatedDoesNotExists() throws EtResourceNotFoundException {
        coordinatesService.deleteCoordinates(coordinates.getCoordinatesId());
        fail("EtResourceNotFoundException not thrown at testDeleteCoordinatesWhenCoordinatedDoesNotExists");
    }

    @Test
    public void testGetCoordinatesRepoCount() {
        when(coordinatesRepository.count()).thenReturn(10L);
        assertEquals(10, coordinatesService.getCoordinatesRepoCount());
    }

    @Test(expected = EtResourceNotFoundException.class)
    public void testGetCoordinatesRepoCountWhenEmpty() {
        coordinatesService.getCoordinatesRepoCount();
        fail("EtResourceNotFoundException not thrown at testGetCoordinatesRepoCountWhenEmpty");
    }

}