package com.notorious8.treasurehunt.services;

import com.notorious8.treasurehunt.model.Player;
import com.notorious8.treasurehunt.repositories.PlayerRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;
import org.mockito.junit.MockitoJUnitRunner;


import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PlayerServiceTest {
    @InjectMocks
    private PlayerService service;

    @Mock
    private PlayerRepository repository;

    private Player player;
    private List<Player> playerList;
    private UUID randomId;

    @Before
    public void setUp() {
        player = new Player(UUID.randomUUID(), "John", "Johns", "jojo","john@gmail.com", "123456");
        playerList = Arrays.asList(player, new Player(UUID.randomUUID(), "AAA", "bbb", "abab","aaabbb@gmail.com", "123456"));
        randomId = UUID.randomUUID();
    }

    @Test
    public void testFindByEmailAndPasswordWhenPlayerExists() throws Exception {
        when(repository.findByEmailAndPassword(player.getEmail(), player.getPassword())).thenReturn(player);
        assertEquals(player.getPlayerId(), service.findByEmailAndPassword(player.getEmail(), player.getPassword()).getPlayerId());
    }

    @Test(expected = Exception.class)
    public void testFindByEmailAndPasswordWhenPlayerDoesNotExists() throws Exception {
        assertEquals(player.getPlayerId(), service.findByEmailAndPassword(player.getEmail(), player.getPassword()).getPlayerId());
    }

    @Test
    public void testGetAllPlayersFromRepository() throws Exception {
        when(repository.count()).thenReturn(2L);
        when(repository.findAll()).thenReturn(playerList);
        assertEquals(playerList.size(), service.getAllPlayersFromRepository().size());
    }

    @Test(expected = Exception.class)
    public void testGetAllPlayerFromRepositoryWhenThereAreNoPlayers() throws Exception {
        service.getAllPlayersFromRepository();
        fail("Exception not thrown at testGetAllPlayerFromRepositoryWhenThereAreNoPlayers");
    }

    @Test
    public void testCreatePlayerWhenEmailDoesNotExists() throws Exception {
        when(repository.findByEmail(anyString())).thenReturn(null);
        assertTrue(service.createPlayer(player));
    }

    @Test
    public void testCreatePlayerWhenEmailExists() throws Exception {
        when(repository.findByEmail(anyString())).thenReturn(player);
        assertFalse(service.createPlayer(player));
    }

    @Test
    public void testUpdatePlayerWhenPlayerExists() throws Exception {
        Player updatedPlayer = new Player(player.getPlayerId(), "Konstantinos", "Kouts","SpodX", "kouts@gmai.com", "123456");
        when(repository.findById(player.getPlayerId())).thenReturn(Optional.ofNullable((player)));
        assertTrue(new ReflectionEquals(updatedPlayer).matches(service.updatePlayer(player.getPlayerId(), updatedPlayer)));
    }
    @Test(expected = Exception.class)
    public void testUpdatePlayerWhenEmailExists() throws Exception {
        Player updatedPlayer = playerList.get(1);
        when(repository.findById(player.getPlayerId())).thenReturn(Optional.ofNullable((player)));
        when(repository.findByEmail(updatedPlayer.getEmail())).thenReturn(new Player(UUID.randomUUID(), "Konstantinos", "Kouts","SpodX", "kouts@gmai.com", "123456"));
        service.updatePlayer(player.getPlayerId(),updatedPlayer);
        fail("Exception not thrown at testUpdatePlayerWhenEmailExists ");
    }

    @Test(expected = Exception.class)
    public void testUpdatePlayerWhenPlayerDoesNotExists() throws Exception {
        service.updatePlayer(randomId, new Player(UUID.randomUUID(), "George", "Jacob", "GeoJ","george@gmail.com", "123456"));
        fail("Exception not thrown at testUpdatePlayerWhenPlayerDoesNotExists");
    }

    @Test
    public void testGetPlayerByIdWhenPlayerExists() throws Exception {
        when(repository.findById(player.getPlayerId())).thenReturn(Optional.ofNullable(player));
        assertEquals(player, service.getPlayerById(player.getPlayerId()));
    }

    @Test(expected = Exception.class)
    public void testGetPlayerByIdWhenPlayerDoesNotExists() throws Exception {
        service.getPlayerById(randomId);
        fail("AssertionError not thrown");

    }

    @Test
    public void testDeletePlayerWhenPlayerExists() throws Exception {
        when(repository.findById(player.getPlayerId())).thenReturn(Optional.ofNullable(player));
        assertTrue(service.deletePlayer(player.getPlayerId()));
    }

    @Test(expected = Exception.class)
    public void testDeletePlayerWhenPlayerDoesNotExists() throws Exception {
        service.deletePlayer(randomId);
        fail("Exception not thrown at testDeletePlayerWhenPlayerDoesNotExists");

    }

    @After
    public void tearDown() {
        player = null;
        playerList = null;
        randomId = null;
    }
}