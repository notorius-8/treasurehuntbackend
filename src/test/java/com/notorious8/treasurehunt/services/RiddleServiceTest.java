package com.notorious8.treasurehunt.services;

import com.notorious8.treasurehunt.dto.RiddleDto;
import com.notorious8.treasurehunt.exceptions.EtAuthException;
import com.notorious8.treasurehunt.exceptions.EtResourceNotFoundException;
import com.notorious8.treasurehunt.model.Coordinates;
import com.notorious8.treasurehunt.model.Riddle;
import com.notorious8.treasurehunt.repositories.RiddleRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RiddleServiceTest {

    @InjectMocks
    private RiddleService riddleService;

    @Mock
    private CoordinatesService coordinatesService;

    @Mock
    private RiddleRepository riddleRepository;

    private Riddle firstRiddle;
    private List<Riddle> riddleList;
    private List<Coordinates> coordinatesList;
    private List<RiddleDto> riddleDtoList;

    @Before
    public void setUp() {
        firstRiddle = new Riddle(UUID.randomUUID(), "number", "1", "a", "b", "c", 10);
        Riddle secondRiddle = new Riddle(UUID.randomUUID(), "word", "a", "1", "2", "3", 10);
        riddleList = Arrays.asList(firstRiddle, secondRiddle);
        Coordinates firstCoordinates = new Coordinates(UUID.randomUUID(), 41.0752080982, 23.5543570414);
        Coordinates secondCoordinates = new Coordinates(UUID.randomUUID(), 41.0752080987, 23.5543570454);
        coordinatesList = Arrays.asList(firstCoordinates, secondCoordinates);
        RiddleDto firstRiddleDto = firstRiddle.convertToRiddleDto(firstCoordinates);
        RiddleDto secondRiddleDto = secondRiddle.convertToRiddleDto(secondCoordinates);
        riddleDtoList = Arrays.asList(firstRiddleDto, secondRiddleDto);
    }

    @After
    public void tearDown() {
        firstRiddle = null;
        riddleList = null;
        riddleDtoList = null;
        coordinatesList = null;
    }

    @Test
    public void testGetAListOfAllRiddlesWhenThereAreRiddles() throws EtAuthException {
        when(riddleRepository.count()).thenReturn(2L);
        when(riddleRepository.findAll()).thenReturn(riddleList);
        assertTrue(new ReflectionEquals(riddleService.getAListOfAllRiddles()).matches(riddleList));
    }

    @Test(expected = EtAuthException.class)
    public void testGetAListOfAllRiddlesWhenThereAreNoRiddles() throws EtAuthException {
        riddleService.getAListOfAllRiddles();
        fail("EtAuthException not thrown at testGetAListOfAllRiddlesWhenThereAreNoRiddles");
    }

    @Test
    public void testGetAListOfRandomRiddlesWhenThereAreRiddles() throws EtAuthException {
        when(riddleRepository.count()).thenReturn(2L);
        when(riddleRepository.getRandomRiddles(2)).thenReturn(riddleList);
        assertTrue(new ReflectionEquals(riddleService.getAListOfRandomRiddles(2)).matches(riddleList));
    }

    @Test(expected = EtAuthException.class)
    public void testGetAListOfRandomRiddlesWhenThereAreNoRiddles() throws EtAuthException {
        when(riddleRepository.count()).thenReturn(10L);
        riddleService.getAListOfRandomRiddles(15);
        fail("EtAuthException not thrown at getAListOfRandomRiddlesThrownExceptionTest");
    }

    @Test
    public void testGetRiddleByIdTestWhenRiddleExists() throws EtResourceNotFoundException {
        UUID id = firstRiddle.getRiddleId();
        when(riddleRepository.findById(id)).thenReturn(Optional.ofNullable(firstRiddle));
        assertTrue(new ReflectionEquals(riddleService.getRiddleById(id)).matches(firstRiddle));
    }

    @Test(expected = EtResourceNotFoundException.class)
    public void testGetRiddleByIdWhenRiddlesDoesNotExists() {
        riddleService.getRiddleById(firstRiddle.getRiddleId());
        fail("EtResourceNotFoundException not thrown at getRiddleByIdThrownExceptionTest");
    }

    @Test
    public void testCreateRiddleTest() throws EtAuthException {
        when(riddleRepository.save(firstRiddle)).thenReturn(firstRiddle);
        when(riddleRepository.findRiddleByRiddle(firstRiddle.getRiddle())).thenReturn(null);
        assertEquals(firstRiddle.getRiddleId(), riddleService.createRiddle(firstRiddle).getRiddleId());
    }

    @Test(expected = EtAuthException.class)
    public void testCreateRiddleWhenRiddleExists() throws EtAuthException {
        when(riddleRepository.findRiddleByRiddle(firstRiddle.getRiddle())).thenReturn(firstRiddle);
        riddleService.createRiddle(firstRiddle);
        fail("EtAuthException not thrown at createRiddleThrownExceptionTest");
    }

    @Test
    public void testUpdateRiddleTest() throws EtResourceNotFoundException {
        UUID id = firstRiddle.getRiddleId();
        Riddle newRiddle = new Riddle(firstRiddle.getRiddleId(), "Two Words", "aa", "1", "c", "2", 10);
        when(riddleRepository.findById(id)).thenReturn(java.util.Optional.of(firstRiddle));
        when(riddleRepository.save(firstRiddle)).thenReturn(newRiddle);
        assertTrue(new ReflectionEquals(riddleService.updateRiddle(id, newRiddle)).matches(firstRiddle));
    }

    @Test(expected = EtResourceNotFoundException.class)
    public void testUpdateRiddleWhenRiddleDoesNotExists() {
        riddleService.updateRiddle(firstRiddle.getRiddleId(), new Riddle(firstRiddle.getRiddleId(), firstRiddle.getRiddle(), firstRiddle.getCorrectAnswer(), firstRiddle.getAnswer2(), firstRiddle.getAnswer3(), firstRiddle.getAnswer4(), firstRiddle.getPoints()));
        fail("EtResourceNotFoundException not thrown at updateRiddleExceptionThrownTest");
    }

    @Test
    public void testDeleteRiddleWhenRiddleExists() throws EtResourceNotFoundException {
        UUID id = firstRiddle.getRiddleId();
        when(riddleRepository.findById(id)).thenReturn(java.util.Optional.of(firstRiddle));
        assertTrue(riddleService.deleteRiddle(firstRiddle.getRiddleId()));
    }

    @Test(expected = EtResourceNotFoundException.class)
    public void testDeleteRiddleWhenRiddleDoesNotExists() {
        riddleService.deleteRiddle(firstRiddle.getRiddleId());
        fail("EtResourceNotFoundException not thrown at deleteRiddleWhenRiddleDoesNotExists");
    }

    @Test
    public void testGetRiddleDtosWhenRiddleExists() throws EtResourceNotFoundException {
        when(riddleRepository.count()).thenReturn(10L);
        when(coordinatesService.getCoordinatesRepoCount()).thenReturn(10L);
        when(riddleService.getAListOfRandomRiddles(2)).thenReturn(riddleList);
        when(coordinatesService.getRandomCoordinates(2)).thenReturn(coordinatesList);
        assertTrue(new ReflectionEquals(riddleDtoList.toArray()).matches(riddleService.getRiddleDtos(2).toArray()));
    }

    @Test(expected = EtResourceNotFoundException.class)
    public void testGetRiddleDtosWhenRiddleDoesNotExists() throws EtResourceNotFoundException {
        riddleService.getRiddleDtos(2);
        fail("EtResourceNotFoundException not thrown at testGetRiddleDtosWhenRiddleDoesNotExists");
    }

}