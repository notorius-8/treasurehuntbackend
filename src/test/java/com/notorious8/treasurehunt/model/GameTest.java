package com.notorious8.treasurehunt.model;

import com.notorious8.treasurehunt.dto.*;
import liquibase.pro.packaged.G;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;

import java.util.Arrays;
import java.util.UUID;

import static org.junit.Assert.*;

public class GameTest {
    private Game game;
    private Player player;
    private int points;

    @Before
    public void setUp() {
        game = new Game();
        game.setRiddleList(Arrays.asList(new RiddleDto(), new RiddleDto()));
        player = new Player(UUID.randomUUID(), "John", "Johns","jojo", "john@gmail.com", "123456");
        game.addPlayer(player.convertToPlayerGameInfoDto());
        points = 10;
    }

    @After
    public void tearDown() {
        game = null;
        player=null;
    }

    @Test
    public void testAddPointsToPlayerWhenPlayerExists() throws Exception {
        Player player = new Player(UUID.randomUUID(), "John", "Johns","jojo" ,"john@gmail.com", "123456");
        game.addPlayer(player.convertToPlayerGameInfoDto());
        assertTrue(new ReflectionEquals(game.addPointsToPlayer(player.getPlayerId(), points)).matches(points));
    }

    @Test
    public void testAddPlayer(){
        assertTrue(game.addPlayer(new Player(UUID.randomUUID(), "Joy", "Joys","jojo" ,"joy@gmail.com", "123456").convertToPlayerGameInfoDto()));
    }
    @Test
    public void testAddPlayerWhenPlayerInGame(){
        assertFalse(game.addPlayer(player.convertToPlayerGameInfoDto()));
    }

    @Test(expected = Exception.class)
    public void testAddPointsToPlayerWhenPlayerDoesNotExists() throws Exception {
        assertTrue(new ReflectionEquals(game.addPointsToPlayer(UUID.randomUUID(), points)).matches(points));
        fail("Exception not thrown at addPointsToPlayerWhenPlayerDoesNotExists");
    }

    @Test
    public void testConvertToGameInfoDto() {
        GameInfoDto gameInfoDto = new GameInfoDto();
        gameInfoDto.setGameId(game.getId());
        gameInfoDto.setNumberOfPlayers(game.getPlayerList().size());
        gameInfoDto.setNumberOfRiddles(game.getRiddleList().size());
        assertTrue(new ReflectionEquals(gameInfoDto).matches(game.convertToGameInfoDto()));

    }

    @Test
    public void testConvertToCreateGameDto() {
        CreateGameDto createGameDto = new CreateGameDto();
        createGameDto.setGameId(game.getId());
        createGameDto.setRiddleDtoList(game.getRiddleList());
        createGameDto.setScoreToWin(game.getScoreToWin());
        assertTrue(new ReflectionEquals(createGameDto).matches(game.convertToCreateGameDto()));
    }

    @Test
    public void testConvertToGameStatusDto() {
        GameStatusDto gameStatusDto = new GameStatusDto();
        gameStatusDto.setFinished(game.isFinished());
        gameStatusDto.setWinner(game.getWinner());
        assertTrue(new ReflectionEquals(gameStatusDto).matches(game.convertToGameStatusDto()));
    }

    @Test
    public void testConvertToJoinGameDto() {
        JoinGameDto joinGameDto = new JoinGameDto();
        joinGameDto.setScoreToWin(game.getScoreToWin());
        joinGameDto.setRiddleDtoList(game.getRiddleList());
        assertTrue(new ReflectionEquals(joinGameDto).matches(game.convertToJoinGameDto()));

    }
}