package com.notorious8.treasurehunt.model;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;

import java.util.UUID;

import static org.junit.Assert.*;

public class CoordinatesTest {

    private Coordinates coordinates;

    @Before
    public void setUp() {
        coordinates = new Coordinates(UUID.randomUUID(),41.0752080982,23.5543570414);
    }

    @After
    public void tearDown() {
        coordinates = null;
    }

    @Test
    public void testCopyCoordinates() {
        Coordinates coordinatesDetails = new Coordinates(coordinates.getCoordinatesId(),41.0764359843,23.8790123583);
        coordinates.copyCoordinates(coordinatesDetails);
        assertTrue(new ReflectionEquals(coordinates).matches(coordinatesDetails));
    }
}