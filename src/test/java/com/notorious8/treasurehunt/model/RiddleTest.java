package com.notorious8.treasurehunt.model;

import com.notorious8.treasurehunt.dto.RiddleDto;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;

import java.util.UUID;

import static org.junit.Assert.*;

public class RiddleTest {

    private Riddle riddle;

    @Before
    public void setUp() {
        riddle = new Riddle(UUID.randomUUID(), "number", "1", "a", "b", "c", 10);
    }

    @After
    public void tearDown() {
        riddle = null;
    }

    @Test
    public void testCopyRiddle() {

        Riddle riddleDetails = new Riddle(riddle.getRiddleId(), "word", "a", "1", "2", "3", 10);
        riddle.copyRiddle(riddleDetails);
        assertTrue(new ReflectionEquals(riddle).matches(riddleDetails));
    }

    @Test
    public void testConvertToRiddleDto() {
        Coordinates coordinates = new Coordinates(UUID.randomUUID(), 41.0752080982, 23.5543570414);
        RiddleDto riddleDto = new RiddleDto();
        riddleDto.setRiddle(riddle.getRiddle());
        riddleDto.setCorrectAnswer(riddle.getCorrectAnswer());
        riddleDto.setAnswer2(riddle.getAnswer2());
        riddleDto.setAnswer3(riddle.getAnswer3());
        riddleDto.setAnswer4(riddle.getAnswer4());
        riddleDto.setPoints(riddle.getPoints());
        riddleDto.setLatitude(coordinates.getLatitude());
        riddleDto.setLongitude(coordinates.getLongitude());
        assertTrue(new ReflectionEquals(riddleDto).matches(riddle.convertToRiddleDto(coordinates)));

    }
}