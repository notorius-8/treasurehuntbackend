package com.notorious8.treasurehunt.model;

import com.notorious8.treasurehunt.dto.PlayerGameInfoDto;
import com.notorious8.treasurehunt.dto.PlayerLoginDto;
import com.notorious8.treasurehunt.dto.PlayerProgressInfoDto;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;

import java.util.UUID;

import static org.junit.Assert.*;

public class PlayerTest {

    private Player player;

    @Before
    public void setUp() throws Exception {
        player = new Player(UUID.randomUUID(), "John", "Johns", "Jojo", "john@gmail.com", "123456");
    }

    @After
    public void tearDown() throws Exception {
        player = null;
    }

    @Test
    public void testCopyPlayer() {
        Player playerDetails = new Player(player.getPlayerId(), "Jason", "Mpou", "Json", "jason@gmai.com", "123456");
        player.copyPlayer(playerDetails);
        assertTrue(new ReflectionEquals(playerDetails).matches(player));
    }

    @Test
    public void testConvertToPlayerGameInfoDto() {
        PlayerGameInfoDto playerGameInfoDto = new PlayerGameInfoDto();
        playerGameInfoDto.setPlayerId(player.getPlayerId());
        playerGameInfoDto.setUserName(player.getUserName());
        playerGameInfoDto.setPoints(0);
        assertTrue(new ReflectionEquals(playerGameInfoDto).matches(player.convertToPlayerGameInfoDto()));
    }

    @Test
    public void testConvertToPlayerLoginDto() {
        PlayerLoginDto playerLoginDto = new PlayerLoginDto();
        playerLoginDto.setPlayerId(player.getPlayerId());
        playerLoginDto.setUserName(player.getUserName());
        assertTrue(new ReflectionEquals(playerLoginDto).matches(player.convertToPlayerLoginDto()));
    }
}