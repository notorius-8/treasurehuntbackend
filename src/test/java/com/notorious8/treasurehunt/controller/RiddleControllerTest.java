package com.notorious8.treasurehunt.controller;

import com.notorious8.treasurehunt.dto.RiddleDto;
import com.notorious8.treasurehunt.model.Coordinates;
import com.notorious8.treasurehunt.model.Riddle;
import com.notorious8.treasurehunt.services.RiddleService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.sql.Ref;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RiddleControllerTest {

    @InjectMocks
    private RiddleController riddleController;

    @Mock
    private RiddleService riddleService;

    private Riddle firstRiddle;
    private Riddle secondRiddle;
    private List<Riddle> riddleList;
    private List<RiddleDto> riddleDtoList;

    @Before
    public void setUp() {
        firstRiddle = new Riddle(UUID.randomUUID(), "word", "a", "1", "2", "3", 10);
        secondRiddle = new Riddle(UUID.randomUUID(), "number", "1", "a", "b", "c", 10);
        riddleList = Arrays.asList(firstRiddle, secondRiddle);
        Coordinates firstCoordinates = new Coordinates(UUID.randomUUID(), 41.0752080987, 23.5543570454);
        Coordinates secondCoordinates = new Coordinates(UUID.randomUUID(), 41.0752080982, 23.5543570414);
        List<Coordinates> coordinatesList = Arrays.asList(firstCoordinates, secondCoordinates, firstCoordinates, secondCoordinates, firstCoordinates, secondCoordinates);
        RiddleDto firstRiddleDto = firstRiddle.convertToRiddleDto(firstCoordinates);
        riddleDtoList = new ArrayList<>();
        riddleDtoList.add(firstRiddleDto);
        RiddleDto secondRiddleDto = secondRiddle.convertToRiddleDto(secondCoordinates);
        riddleDtoList.add(secondRiddleDto);

    }

    @Test
    public void testGetAListOfAllRiddles() {
        when(riddleService.getAListOfAllRiddles()).thenReturn(riddleList);
        assertTrue(new ReflectionEquals(riddleController.getAllRiddles()).matches(riddleList));
    }

    @Test
    public void testGetRiddleById() {
        when(riddleService.getRiddleById(firstRiddle.getRiddleId())).thenReturn(firstRiddle);
        ResponseEntity<Riddle> responseEntity = riddleController.getRiddleById(firstRiddle.getRiddleId());
        assertTrue(new ReflectionEquals(responseEntity.getStatusCode()).matches(HttpStatus.OK));
        assertTrue(new ReflectionEquals(responseEntity.getBody()).matches(firstRiddle));
    }

    @Test
    public void testGetRandomRiddles() {

        when(riddleService.getAListOfRandomRiddles(2)).thenReturn(riddleList);
        assertTrue(new ReflectionEquals(riddleController.getRandomRiddles(2)).matches(riddleList));
    }

    @Test
    public void testCreateRiddle() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        Riddle thirdRiddle = new Riddle(UUID.randomUUID(), "Two Words", "aa", "1", "a", "v", 10);
        when(riddleService.createRiddle(thirdRiddle)).thenReturn(thirdRiddle);
        ResponseEntity<Riddle> responseEntity = riddleController.createRiddle(thirdRiddle);
        assertTrue(new ReflectionEquals(responseEntity.getStatusCode()).matches(HttpStatus.OK));
        assertTrue(new ReflectionEquals(responseEntity.getBody()).matches(thirdRiddle));
    }

    @Test
    public void testUpdateRiddle() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        Riddle thirdRiddle = new Riddle(UUID.randomUUID(), "Two Words", "aa", "1", "a", "v", 10);
        when(riddleService.updateRiddle(secondRiddle.getRiddleId(), thirdRiddle)).thenReturn(thirdRiddle);
        ResponseEntity<Riddle> responseEntity = riddleController.updateRiddle(secondRiddle.getRiddleId(), thirdRiddle);
        assertTrue(new ReflectionEquals(responseEntity.getStatusCode()).matches(HttpStatus.OK));
        assertTrue(new ReflectionEquals(responseEntity.getBody()).matches(thirdRiddle));

    }

    @Test
    public void testDeleteRiddle() {
        Riddle thirdRiddle = new Riddle(UUID.randomUUID(), "Two Words", "aa", "1", "a", "v", 10);
        when(riddleService.deleteRiddle(thirdRiddle.getRiddleId())).thenReturn(true);
        boolean response = riddleController.deleteRiddle(thirdRiddle.getRiddleId());
        assertTrue(response);

    }

    @Test
    public void testGetRiddleDtos() {
        when(riddleService.getRiddleDtos(2)).thenReturn(riddleDtoList);
        assertTrue(new ReflectionEquals(riddleController.getRiddleDtos(2)).matches(riddleDtoList));
    }

}
