package com.notorious8.treasurehunt.controller;

import com.notorious8.treasurehunt.dto.*;
import com.notorious8.treasurehunt.model.Game;
import com.notorious8.treasurehunt.model.Player;
import com.notorious8.treasurehunt.services.GameService;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.sql.Ref;
import java.util.*;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class GameControllerTest {

    @Mock
    private GameService gameService;
    @InjectMocks
    private GameController gameController;

    private Game firstGame;
    private Game secondGame;
    private List<GameInfoDto> gameList;
    private List<RiddleDto> riddleDtoList = new ArrayList<>();
    private final Player player = new Player(UUID.randomUUID(), "Jason", "Mpouzgos", "Json", "jason12@gmail.com", "1234567f");
    private final GameStatusDto gameStatusDto = new GameStatusDto();
    private int points;

    @Before
    public void setUp() {
        firstGame = new Game();
        secondGame = new Game();
        gameList = Arrays.asList(firstGame.convertToGameInfoDto(), secondGame.convertToGameInfoDto());
        riddleDtoList = Arrays.asList(new RiddleDto(), new RiddleDto());
        points = 10;
    }

    @After
    public void tearDown() {
        firstGame = null;
        secondGame = null;
        gameList = null;
        riddleDtoList = null;
    }

    @Test
    public void testGetAllGames() throws Exception {
        when(gameService.getAllGames()).thenReturn(gameList);
        assertTrue(new ReflectionEquals(gameController.getAllGames()).matches(gameList));
    }

    @Test
    public void testCreateGame() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        Game thirdGame = new Game();

        when(gameService.createGame(5, player.getPlayerId())).thenReturn(thirdGame.convertToCreateGameDto());

        ResponseEntity<CreateGameDto> responseEntity = gameController.createGame(5, player.getPlayerId());

        assertTrue(new ReflectionEquals(responseEntity.getStatusCode()).matches(HttpStatus.OK));
        assertTrue(new ReflectionEquals(responseEntity.getBody()).matches(thirdGame.convertToCreateGameDto()));
    }

    @Test
    public void testFindGameById() throws Exception {
        when(gameService.findGameById(firstGame.getId())).thenReturn(firstGame);
        assertTrue(new ReflectionEquals(gameController.findGameById(firstGame.getId())).matches(firstGame));
    }

    @Test
    public void testJoinGame() throws Exception {
        firstGame.setRiddleList(Arrays.asList(new RiddleDto(), new RiddleDto()));
        when(gameService.joinGame(firstGame.getId(), player.getPlayerId())).thenReturn(firstGame.convertToJoinGameDto());
        assertTrue(new ReflectionEquals(gameController.joinGame(firstGame.getId(), player.getPlayerId())).matches(firstGame.convertToJoinGameDto()));
    }

    @Test
    public void testDeleteGame() throws Exception {
        when(gameService.deleteGame(firstGame.getId())).thenReturn(true);
        assertTrue(gameController.deleteGame(firstGame.getId()));
    }

    @Test
    public void testGetGameRiddles() throws Exception {
        when(gameService.getGameRiddles(firstGame.getId())).thenReturn(riddleDtoList);
        assertTrue(new ReflectionEquals(gameController.getGameRiddles(firstGame.getId())).matches(riddleDtoList));
    }

    @Test
    public void testAddPointsToPlayer() throws Exception {
        when(gameService.addPointsToPlayer(firstGame.getId(), player.getPlayerId(), 10)).thenReturn(10);
        assertTrue(new ReflectionEquals(gameController.addPointsToPlayer(firstGame.getId(), player.getPlayerId(), points)).matches(points));
    }

    @Test
    public void testGetGameStatus() throws Exception {
        when(gameService.getGameStatus(firstGame.getId())).thenReturn(gameStatusDto);
        assertFalse(gameController.getGameStatus(firstGame.getId()).isFinished());
    }

    @Test(expected = Exception.class)
    public void testGetGameStatusWhenThereIsNoGame() throws Exception {
        assertFalse(gameController.getGameStatus(firstGame.getId()).isFinished());
    }

    @Test
    public void testGetPlayersInGameInfo() throws Exception {
        List<PlayerProgressInfoDto> playerProgressInfoDtos = new ArrayList<>();
        for (PlayerGameInfoDto playerGameInfoDto : firstGame.getPlayerList()){
            playerProgressInfoDtos.add(playerGameInfoDto.convertToPlayerProgressInfoDto());
        }
        when(gameService.getPlayersInGameInfo(firstGame.getId())).thenReturn(playerProgressInfoDtos);
        assertNotNull(gameController.getPlayersInGameInfo(firstGame.getId()));
    }
}