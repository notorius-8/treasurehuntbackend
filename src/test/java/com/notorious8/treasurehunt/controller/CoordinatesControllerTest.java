package com.notorious8.treasurehunt.controller;

import com.notorious8.treasurehunt.model.Coordinates;
import com.notorious8.treasurehunt.services.CoordinatesService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CoordinatesControllerTest {

    @InjectMocks
    private CoordinatesController coordinatesController;

    @Mock
    private CoordinatesService coordinatesService;

    private Coordinates firstCoordinates;
    private Coordinates secondCoordinates;
    private List<Coordinates> coordinatesList;

    @Before
    public void setUp() {
        firstCoordinates = new Coordinates(UUID.randomUUID(), 41.0752080982, 23.5543570414);
        secondCoordinates = new Coordinates(UUID.randomUUID(), 41.0752080987, 23.5543570454);
        coordinatesList = Arrays.asList(firstCoordinates, secondCoordinates);
    }

    @Test
    public void testGetAllCoordinatesWhenThereAreCoordinates() {
        when(coordinatesService.getAListOfAllCoordinates()).thenReturn(coordinatesList);
        assertTrue(new ReflectionEquals(coordinatesController.getAllCoordinates()).matches(coordinatesList));
    }

    @Test
    public void testGetCoordinatesById() {
        when(coordinatesService.getCoordinatesById(firstCoordinates.getCoordinatesId())).thenReturn(firstCoordinates);
        ResponseEntity<Coordinates> responseEntity = coordinatesController.getCoordinatesById(firstCoordinates.getCoordinatesId());
        assertTrue(new ReflectionEquals(responseEntity.getStatusCode()).matches(HttpStatus.OK));
        assertTrue(new ReflectionEquals(responseEntity.getBody()).matches(firstCoordinates));
    }

    @Test
    public void testGetRandomCoordinates() {
        when(coordinatesService.getRandomCoordinates(2)).thenReturn(coordinatesList);
        assertTrue(new ReflectionEquals(coordinatesController.getRandomCoordinates(2)).matches(coordinatesList));
    }

    @Test
    public void testCreateCoordinates() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        Coordinates thirdCoordinates = new Coordinates(UUID.randomUUID(), 41.0752780987, 23.5548570454);

        when(coordinatesService.createCoordinates(thirdCoordinates)).thenReturn(thirdCoordinates);

        ResponseEntity<Coordinates> responseEntity = coordinatesController.createCoordinates(thirdCoordinates);

        assertTrue(new ReflectionEquals(responseEntity.getStatusCode()).matches(HttpStatus.OK));
        assertTrue(new ReflectionEquals(responseEntity.getBody()).matches(thirdCoordinates));
    }

    @Test
    public void testUpdateCoordinates() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        Coordinates thirdCoordinates = new Coordinates(UUID.randomUUID(), 41.0752780987, 23.5548570454);

        when(coordinatesService.updateCoordinates(secondCoordinates.getCoordinatesId(), thirdCoordinates)).thenReturn(thirdCoordinates);

        ResponseEntity<Coordinates> responseEntity = coordinatesController.updateCoordinates(secondCoordinates.getCoordinatesId(), thirdCoordinates);

        assertTrue(new ReflectionEquals(responseEntity.getStatusCode()).matches(HttpStatus.OK));
        assertTrue(new ReflectionEquals(responseEntity.getBody()).matches(thirdCoordinates));
    }

    @Test
    public void testDeleteCoordinates() {
        Coordinates thirdCoordinates = new Coordinates(UUID.randomUUID(), 41.0752780987, 23.5548570454);

        when(coordinatesService.deleteCoordinates(thirdCoordinates.getCoordinatesId())).thenReturn(true);

        boolean response = coordinatesController.deleteCoordinates(thirdCoordinates.getCoordinatesId());
        assertTrue(response);
    }
}