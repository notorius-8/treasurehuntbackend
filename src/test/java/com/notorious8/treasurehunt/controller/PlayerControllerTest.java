package com.notorious8.treasurehunt.controller;


import com.notorious8.treasurehunt.model.Player;

import com.notorious8.treasurehunt.services.PlayerService;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


import java.util.*;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PlayerControllerTest {

    @Mock
    private PlayerService playerService;
    @InjectMocks
    private PlayerController playerController;

    private Player firstPlayer;
    private Player secondPlayer;
    private List<Player> playerList;

    @Before
    public void setUp() {
        firstPlayer = new Player(UUID.randomUUID(), "Jason", "Mpouzgos", "Json", "jason12@gmail.com", "1234567f");
        secondPlayer = new Player(UUID.randomUUID(), "Kostas", "Koutsilis", "SpodX", "kouts@gmail.com", "1234567f");
        playerList = Arrays.asList(firstPlayer, secondPlayer);
    }

    @After
    public void tearDown() {
        firstPlayer = null;
        secondPlayer = null;
        playerList = null;
    }

    @Test
    public void testGetAllPlayers() throws Exception {
        when(playerService.getAllPlayersFromRepository()).thenReturn(playerList);
        assertTrue(new ReflectionEquals(playerController.getAllPlayers()).matches(playerList));
    }

    @Test
    public void testGetPlayerById() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        when(playerService.getPlayerById(firstPlayer.getPlayerId())).thenReturn(firstPlayer);

        ResponseEntity<Player> responseEntity = playerController.getPlayerById(firstPlayer.getPlayerId());

        assertTrue(new ReflectionEquals(responseEntity.getStatusCode()).matches(HttpStatus.OK));
        assertTrue(new ReflectionEquals(responseEntity.getBody()).matches(firstPlayer));
    }

    @Test
    public void testGetPlayerByEmailAndPassword() throws Exception {
        when(playerService.findByEmailAndPassword(firstPlayer.getEmail(), firstPlayer.getPassword())).thenReturn(firstPlayer.convertToPlayerLoginDto());
        assertTrue(new ReflectionEquals(firstPlayer.convertToPlayerLoginDto()).matches(playerController.getPlayerByEmailAndPassword(firstPlayer.getEmail(),firstPlayer.getPassword())));
    }

    @Test
    public void testCreatePlayer() throws Exception {
        Player thirdPlayer = new Player(UUID.randomUUID(), "Dim", "Kanel","DimKan", "kanel@gmail.com", "12345567");
        when(playerService.createPlayer(thirdPlayer)).thenReturn(true);
        assertTrue(playerController.createPlayer(thirdPlayer));

    }

    @Test
    public void testUpdatePlayer() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        Player thirdPlayer = new Player(UUID.randomUUID(), "Dim", "Kanel","DimKan", "kanel@gmail.com", "12345567");

        when(playerService.updatePlayer(firstPlayer.getPlayerId(), thirdPlayer)).thenReturn(thirdPlayer);

        ResponseEntity<Player> responseEntity = playerController.updatePlayer(firstPlayer.getPlayerId(), thirdPlayer);

        assertTrue(new ReflectionEquals(responseEntity.getStatusCode()).matches(HttpStatus.OK));
        assertTrue(new ReflectionEquals(responseEntity.getBody()).matches(thirdPlayer));

    }

    @Test
    public void testDeletePlayer() throws Exception {
        when(playerService.deletePlayer(firstPlayer.getPlayerId())).thenReturn(true);
        assertTrue(playerController.deletePlayer(firstPlayer.getPlayerId()));
    }
}
